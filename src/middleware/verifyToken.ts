import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

export interface AuthRequest extends Request {
  userId?: string;
}

const secretKey: string = process.env.JWT_SECRET || "no_key"

export const verifyToken = (req: AuthRequest, res: Response, next: NextFunction) => {
  const token = req.headers.authorization?.split(' ')[1];

  if (!token) {
    return res.status(401).json({ error: 'No token provided' });
  }

  try {
    const decodedToken = jwt.verify(token, secretKey) as { userId: string };

    // Attach the userId to the request object
    req.userId = decodedToken.userId;

    next();
  } catch (error) {
    return res.status(403).json({ error: 'Failed to verify token' });
  }
};
