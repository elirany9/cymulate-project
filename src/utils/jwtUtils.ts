import jwt from 'jsonwebtoken';

const secretKey = process.env.JWT_SECRET || "no_key";

export const generateToken = (userId: string): string => {
  const token = jwt.sign({ userId }, secretKey, { expiresIn: '1h' });
  return token;
};
