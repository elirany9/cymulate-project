import mongoose, { Schema, Document } from 'mongoose';

export interface IFriendship extends Document {
  sender: string;
  receiver: string;
  status: string;
}

const friendshipSchema: Schema = new Schema({
  sender: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  receiver: { type: Schema.Types.ObjectId, ref: 'User', required: true },
  status: { type: String, enum: ['pending', 'accepted', 'rejected'], default: 'pending' },
});

export default mongoose.model<IFriendship>('Friendship', friendshipSchema);