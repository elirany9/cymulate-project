import express from 'express';
import { createPost, updatePost, deletePost, getPost, getFriendPosts } from '../controllers/PostController';
import { verifyToken } from '../middleware/verifyToken';

const router = express.Router();

// Create a post
router.post('/posts', verifyToken, createPost);

// Update a post
router.put('/posts/:postId', verifyToken, updatePost);

// Delete a post
router.delete('/posts/:postId', verifyToken, deletePost);

// Get a post
router.get('/posts/:postId', verifyToken, getPost);

// Get posts of friends
router.get('/posts/friends', getFriendPosts);

export default router;