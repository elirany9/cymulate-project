import express from 'express';
import { createComment, updateComment, deleteComment } from '../controllers/CommentController';
import { verifyToken } from '../middleware/verifyToken';

const router = express.Router();

// Create a comment
router.post('/comments', verifyToken, createComment);

// Update a comment
router.put('/comments/:commentId', verifyToken, updateComment);

// Delete a comment
router.delete('/comments/:commentId', verifyToken, deleteComment);

export default router;