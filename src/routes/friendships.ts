import express from 'express';
import { sendFriendRequest, acceptFriendRequest, rejectFriendRequest, getFriends } from '../controllers/FriendshipController';
import { verifyToken } from '../middleware/verifyToken';

const router = express.Router();

// Send a friend request
router.post('/friend-requests/:receiverId', verifyToken, sendFriendRequest);

// Accept a friend request
router.put('/friend-requests/:requestId/accept', verifyToken, acceptFriendRequest);

// Reject a friend request
router.put('/friend-requests/:requestId/reject', verifyToken, rejectFriendRequest);

// Get the list of friends
router.get('/friends', verifyToken, getFriends);

export default router;
