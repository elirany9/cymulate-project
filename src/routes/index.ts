import express from 'express';
import users from './users';
import posts from './posts';
import comments from './comments'
import friendship from './friendships'

const router = express.Router();

router.use('/users', users);
router.use('/posts', posts)
router.use('/comments', comments)
router.use('/friendships', friendship)

export default router;