import { Request, Response } from 'express';
import User from '../models/User';

export interface AuthRequest extends Request {
  userId?: string;
}

export const sendFriendRequest = async (req: AuthRequest, res: Response) => {
  const { receiverId } = req.params;
  const senderId = req.userId; // Extracted from the authenticated request

  try {
    const sender = await User.findById(senderId);
    if (!sender) {
      return res.status(404).json({ error: 'Sender not found' });
    }

    const receiver = await User.findById(receiverId);
    if (!receiver) {
      return res.status(404).json({ error: 'Receiver not found' });
    }

    // Check if the friend request already exists
    if (sender.friendRequests.includes(receiverId) || receiver.friendRequests.includes(senderId)) {
      return res.status(400).json({ error: 'Friend request already sent or received' });
    }

    // Send friend request
    sender.friendRequests.push(receiverId);
    await sender.save();

    res.status(200).json({ message: 'Friend request sent successfully' });
  } catch (error) {
    res.status(500).json({ error: 'Failed to send friend request' });
  }
};

export const acceptFriendRequest = async (req: AuthRequest, res: Response) => {
  const { requestId } = req.params;
  const receiverId = req.userId; // Extracted from the authenticated request

  try {
    const receiver = await User.findById(receiverId);
    if (!receiver) {
      return res.status(404).json({ error: 'Receiver not found' });
    }

    // Check if the friend request exists
    if (!receiver.friendRequests.includes(requestId)) {
      return res.status(400).json({ error: 'Friend request not found' });
    }

    // Accept friend request
    receiver.friendRequests = receiver.friendRequests.filter(request => request !== requestId);
    receiver.friends.push(requestId);
    await receiver.save();

    res.status(200).json({ message: 'Friend request accepted successfully' });
  } catch (error) {
    res.status(500).json({ error: 'Failed to accept friend request' });
  }
};

export const rejectFriendRequest = async (req: AuthRequest, res: Response) => {
  const { requestId } = req.params;
  const receiverId = req.userId; // Extracted from the authenticated request

  try {
    const receiver = await User.findById(receiverId);
    if (!receiver) {
      return res.status(404).json({ error: 'Receiver not found' });
    }

    // Check if the friend request exists
    if (!receiver.friendRequests.includes(requestId)) {
      return res.status(400).json({ error: 'Friend request not found' });
    }

    // Reject friend request
    receiver.friendRequests = receiver.friendRequests.filter(request => request !== requestId);
    await receiver.save();

    res.status(200).json({ message: 'Friend request rejected successfully' });
  } catch (error) {
    res.status(500).json({ error: 'Failed to reject friend request' });
  }
};

export const getFriends = async (req: AuthRequest, res: Response) => {
  const userId = req.userId; // Extracted from the authenticated request

  try {
    const user = await User.findById(userId).populate('friends', '-password');
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    res.status(200).json({ friends: user.friends });
  } catch (error) {
    res.status(500).json({ error: 'Failed to get friends' });
  }
};
