import { Request, Response } from 'express';
import Post, { IPost } from '../models/Post';
import User from '../models/User';

export interface AuthRequest extends Request {
  userId?: string;
}

export const createPost = async (req: AuthRequest, res: Response) => {
  const { content } = req.body;
  const authorId = req.userId; // Extracted from the authenticated request

  try {
    const author = await User.findById(authorId);
    if (!author) {
      return res.status(404).json({ error: 'Author not found' });
    }

    const newPost: IPost = new Post({
      content,
      author: authorId,
    });

    await newPost.save();

    res.status(201).json({ message: 'Post created successfully', post: newPost });
  } catch (error) {
    res.status(500).json({ error: 'Failed to create post' });
  }
};

export const getPost = async (req: Request, res: Response) => {
  const postId = req.params.postId;

  try {
    const post = await Post.findById(postId).populate('author', '-password');
    if (!post) {
      return res.status(404).json({ error: 'Post not found' });
    }

    res.status(200).json(post);
  } catch (error) {
    res.status(500).json({ error: 'Failed to retrieve post' });
  }
};

export const updatePost = async (req: Request, res: Response) => {
  const postId = req.params.postId;
  const { content } = req.body;

  try {
    const updatedPost = await Post.findByIdAndUpdate(
      postId,
      { content },
      { new: true }
    );

    if (!updatedPost) {
      return res.status(404).json({ error: 'Post not found' });
    }

    res.status(200).json({ message: 'Post updated successfully', post: updatedPost });
  } catch (error) {
    res.status(500).json({ error: 'Failed to update post' });
  }
};

export const deletePost = async (req: Request, res: Response) => {
  const postId = req.params.postId;

  try {
    const deletedPost = await Post.findByIdAndDelete(postId);
    if (!deletedPost) {
      return res.status(404).json({ error: 'Post not found' });
    }

    res.status(200).json({ message: 'Post deleted successfully' });
  } catch (error) {
    res.status(500).json({ error: 'Failed to delete post' });
  }
};

export const getFriendPosts = async (req: AuthRequest, res: Response) => {
  const userId = req.userId; // Extracted from the authenticated request

  try {
    // Find the user by ID and populate the 'friends' field
    const user = await User.findById(userId).populate('friends');
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }

    // Get an array of friend IDs
    const friendIds = user.friends.map(friend => friend.toString());

    // Find posts created by friends
    const friendPosts = await Post.find({ author: { $in: friendIds } });

    res.status(200).json({ friendPosts });
  } catch (error) {
    res.status(500).json({ error: 'Failed to get friend posts' });
  }
};